ODIR=obj
CDIR=src
IDIR=include

CC=g++
CFLAGS=-I$(IDIR) -std=c++17 -g

_OBJ = main.o operator.o string_matching.o tokens.o lexer.o token_matching.o ast.o expression.o interpreter.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR):
	mkdir $@

$(ODIR)/%.o: $(CDIR)/%.cc $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

h1: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean
clean:
	rm -rf $(ODIR)
	rm h1

.PHONY: test
test: h1
	chmod +x main
	./main test_input/test.h1
