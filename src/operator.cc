#include "operator.h"

#include <iostream>

namespace Operator {
	const char* sym[10] = {
		"+", "-", "*", "/", "<", ">", "<=", ">=", "==", "!="
	};

	std::string symbol(size_t idx) {
		return std::string(sym[idx]);
	}

	std::string symbol(Type type) {
		return std::string(sym[(int)type]);
	}

	int order(Type type) {
		return ORDER[(int)type];
	}

	std::map<Type,OperatorFunc> OPERATIONS = {
		std::pair<Type,OperatorFunc>{Type::ADD, [](int a, int b) { return (int) a + b; }},
		std::pair<Type,OperatorFunc>{Type::SUB, [](int a, int b) { return (int) a - b; }},
		std::pair<Type,OperatorFunc>{Type::MUL, [](int a, int b) { return (int) a * b; }},
		std::pair<Type,OperatorFunc>{Type::DIV, [](int a, int b) { return (int) a / b; }},
		std::pair<Type,OperatorFunc>{Type::LT, [](int a, int b) { return (int) a < b; }},
		std::pair<Type,OperatorFunc>{Type::GT, [](int a, int b) { return (int) a > b; }},
		std::pair<Type,OperatorFunc>{Type::LTE, [](int a, int b) { return (int) a <= b; }},
		std::pair<Type,OperatorFunc>{Type::GTE, [](int a, int b) { return (int) a >= b; }},
		std::pair<Type,OperatorFunc>{Type::EQ, [](int a, int b) { return (int) a == b; }},
		std::pair<Type,OperatorFunc>{Type::NE, [](int a, int b) { return (int) a != b; }},
	};

	OperatorFunc operation(Type type) {
		return OPERATIONS[type];
	}
}