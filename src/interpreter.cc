#include "interpreter.h"

#include <map>
#include <functional>
#include <typeindex>
#include <iostream>

namespace Interpreter {
	// class Error : public std::exception {
	// protected:
	// 	int error_number;
	// 	int error_offset;
	// 	std::string error_message;

	// public:
	// 	Error(std::string msg, int num, int off):
	// 		error_number(num),
	// 		error_offset(off),
	// 		error_message(msg) {}

	// 	const char* what() const throw () {
	// 		return error_message.c_str();
	// 	}

	// 	int errno() {
	// 		return error_number;
	// 	}

	// 	int erroff() {
	// 		return error_offset;
	// 	}
	// };

	// int PrintFunction::int_call(int argument) {
	// 	std::cout << argument << std::endl;
	// 	return 0;
	// }

	LambdaFunction::LambdaFunction(std::function<int(int)> func):
		func(func) {}

	int LambdaFunction::int_call(int argument) {
		return func(argument);
	}

	Expression::Function* PRINT_FUNCTION = new LambdaFunction([](int argument) {
		std::cout << argument << std::endl;
		return 0;
	});

	void declare(ScopeInstance* scope, AST::Statement::Ptr statement) {
		auto declaration = (AST::VarDeclaration*) statement.get();
		scope->set_integer(declaration->name, declaration->value->get_integer(scope));
	}

	void conditional(ScopeInstance* scope, AST::Statement::Ptr statement) {
		auto conditional = (AST::Conditional*) statement.get();
		int value = conditional->condition->get_integer(scope);
		if (value != 0) {
			ScopeInstance scopei(conditional->scope, scope);
			scopei.run();
		}
	}

	void call_function(ScopeInstance* scope, AST::Statement::Ptr statement) {
		auto function_call = (AST::FunctionCall*) statement.get();
		auto function = scope->get_function(function_call->name);
		function->int_call(function_call->value->get_integer(scope));
	}

	std::map<std::type_index,std::function<void(ScopeInstance*,AST::Statement::Ptr)>> actions = {
		{std::type_index(typeid(AST::VarDeclaration)), &declare},
		{std::type_index(typeid(AST::Conditional)), &conditional},
		{std::type_index(typeid(AST::FunctionCall)), &call_function},
	};
	
	ScopeInstance::ScopeInstance(AST::Scope::Ptr scope, ScopeInstance* parent):
			scope(scope),
			parent(parent) {}

	void ScopeInstance::run() {
		for (size_t idx=0; idx<scope->statements.size(); idx++) {
			auto statement = scope->statements[idx];
			std::type_index	type(typeid(*statement));
			auto action = actions.find(type);

			if (action != actions.end()) {
				action->second(this, statement);
			}
		}
	}

	std::string ScopeInstance::format() {
		std::string str = "variables:\n";
		for (auto it=integers.begin(); it!=integers.end();it++) {
			str += it->first + " = " + std::to_string(it->second) + "\n";
		}
		return str;
	}

	int ScopeInstance::get_integer(std::string varname) {
		auto var = integers.find(varname);

		if (var != integers.end()) {
			return var->second;
		}

		if (parent != nullptr) {
			return parent->get_integer(varname);
		}

		return 0;
	}

	Expression::Function* ScopeInstance::get_function(std::string funcname) {
		if (funcname=="print") {
			return PRINT_FUNCTION;
		}
		return nullptr;
	}

	void ScopeInstance::set_integer(std::string varname, int value) {
		integers[varname] = value;
	}

	void run(AST::Scope::Ptr scope) {
		ScopeInstance scopei(scope, nullptr);
		scopei.run();
		// std::cout << scopei.format() << std::endl;
	}

}