#include "ast.h"

#include <array>
#include <iostream>
#include <functional>

#include "expression.h"

namespace AST {
	std::string SCOPE_TYPE_NAMES[] = {
		"none", "file", "function", "loop", "conditional"
	};

	Scope::Scope(ScopeType type) : type(type) {}

	void Scope::add_statement(Statement* statement) {
		statements.emplace_back(statement);
	}

	std::string Scope::format(size_t indent) {
		std::string str(indent, '\t');
		str += SCOPE_TYPE_NAMES[(int)type] + " scope";
		for (size_t i=0; i<statements.size(); i++) {
			str += "\n";
			str += std::string(indent, '\t');
			str += "[" + std::to_string(i) + "] ";
			str += statements[i]->format();
		}
		return str;
	}

	VarDeclaration::VarDeclaration(std::string name,Expression::Node::Ptr value)
		: name(name), value(value) {}

	std::string VarDeclaration::format(size_t indent) {
		return std::string(indent, '\t') + "declare variable " + name + ": "+value->format();
	}

	Conditional::Conditional(Expression::Node::Ptr condition) : condition(condition) {
		scope = Scope::Ptr(new Scope(ScopeType::CONDITIONAL));
	}

	std::string Conditional::format(size_t indent) {
		return std::string(indent, '\t') + "conditional " + condition->format() + "\n" + scope->format(indent+1);
	}

	FunctionCall::FunctionCall(std::string name, Expression::Node::Ptr value) : name(name), value(value) {}

	std::string FunctionCall::format(size_t indent) {
		return std::string(indent, '\t') + "call function " + name + " with " + value->format();
	}

	struct MatcherEntry {
		std::string type;
		TokenMatch::Matcher::Ptr matcher;
		std::function<Scope::Ptr(Scope::Ptr,TokenMatch::Match)> scope_modifier;
	};

	MatcherEntry* matchers = new MatcherEntry[4]{
		MatcherEntry{
			"declaration",
			TokenMatch::sequence({
				TokenMatch::string(&Tokens::KEYWORD, "var"),
				TokenMatch::capture(TokenMatch::type(&Tokens::IDENTIFIER)),
				TokenMatch::type(&Tokens::ASSIGNMENT),
				TokenMatch::capture(TokenMatch::expression())
			}),
			[](Scope::Ptr scope, TokenMatch::Match match) {
				auto name_capture = match.capture(0);
				auto value_capture = match.capture(1);

				std::string name = std::any_cast<std::string>(name_capture.data(0));

				auto expression = Expression::parse_expression(value_capture);

				scope->add_statement(new VarDeclaration(name, expression));
				return scope;
			}
		},
		MatcherEntry{
			"conditional",
			TokenMatch::sequence({
				TokenMatch::string(&Tokens::KEYWORD, "if"),
				TokenMatch::capture(TokenMatch::expression())
			}),
			[](Scope::Ptr scope, TokenMatch::Match match) {
				auto conditional_capture = match.capture(0);
				auto condition = Expression::parse_expression(conditional_capture);
				auto statement = new Conditional(condition);
				scope->add_statement(statement);
				statement->scope->parent = scope;
				return statement->scope;
			}
		},
		MatcherEntry{
			"function call",
			TokenMatch::sequence({
				TokenMatch::capture(TokenMatch::type(&Tokens::IDENTIFIER)),
				TokenMatch::type(&Tokens::PAREN_OPEN),
				TokenMatch::capture(TokenMatch::expression()),
				TokenMatch::type(&Tokens::PAREN_CLOSE),
			}),
			[](Scope::Ptr scope, TokenMatch::Match match) {
				auto name = std::any_cast<std::string>(match.capture(0).ptr->data);
				auto value = Expression::parse_expression(match.capture(1));
				scope->add_statement(new FunctionCall(name, value));
				return scope;
			}
		},
		MatcherEntry{
			"scope end",
			TokenMatch::string(&Tokens::KEYWORD, "end"),
			[](Scope::Ptr scope, TokenMatch::Match match) {
				return scope->parent;
			}
		}
	};

	struct MatchResult {
		MatcherEntry* entry;
		TokenMatch::Match match;
	};

	MatchResult get_match(TokenMatch::Token* ptr) {
		for (size_t i=0; i<4; i++) {
			MatcherEntry* entry = &matchers[i];

			auto match = entry->matcher->match(ptr);

			if (match.is_match()) {
				return {entry, match};
			}
		}
		throw "AST: Could not find match.";
	}

	Scope::Ptr create_ast(TokenMatch::Token* token_buffer) {
		auto ptr = token_buffer;
		auto file_scope = std::make_shared<Scope>(ScopeType::FILE);
		auto scope = file_scope;

		while (ptr->type != &Tokens::END) {
			MatchResult result = get_match(ptr);
			scope = result.entry->scope_modifier(
				scope, result.match
			);
			ptr += result.match.len;
		}

		return file_scope;
	}
}
