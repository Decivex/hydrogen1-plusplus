#include "lexer.h"

#include "tokens.h"

#include <array>
#include <optional>
#include <iostream>

namespace Lexer {
	TokenList::TokenList() {
		capacity = 16;
		list = new Tokens::Token[capacity];
	}

	TokenList::~TokenList() {
		delete list;
	}

	const auto types = std::array{
		&Tokens::COMMENT,
		&Tokens::NEWLINE,
		&Tokens::KEYWORD,
		&Tokens::INTEGER,
		&Tokens::OPERATOR,
		&Tokens::ASSIGNMENT,
		&Tokens::IDENTIFIER,
		&Tokens::WHITESPACE,
		&Tokens::PAREN_OPEN,
		&Tokens::PAREN_CLOSE
	};

	void debug(int i) {
		std::cout << "::" << i << "::" << std::endl;
	}

	std::optional<Tokens::Token> get_token(char*& cursor) {
		StringMatch::Match best_match(cursor, 0);
		Tokens::TypePtr best_match_type = &Tokens::NONE;
		for (auto it=types.begin(); it<types.end(); ++it) {
			auto match = (*it)->matcher->match(cursor);
			if (match.is_match() && match.len > best_match.len) {
				best_match = match;
				best_match_type = *it;
			}
		}
		if (best_match.is_match()) {
			Tokens::Token token = Tokens::create_token(cursor, best_match_type, best_match);
			cursor += best_match.len;
			return token;
		}
		else {
			std::cout << "Unkown token: " << *cursor << std::endl;
		}
		cursor++;
		return std::nullopt;
	}

	std::vector<Tokens::Token> parse_buffer(char* cursor, char* end) {
		std::vector<Tokens::Token> tokens;

		while (cursor < end) {
			std::optional<Tokens::Token> token = get_token(cursor);

			if (token.has_value()) {
				if (!token->type->ignore) {
					tokens.push_back(*token);
				}
			}
			else {
				// TODO: Error handling
			}
		}

		tokens.emplace_back(&Tokens::END);

		return tokens;
	}
}