#include <stdio.h>
#include <cctype>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <memory>
#include <filesystem>
#include <vector>

#include "token_matching.h"
#include "lexer.h"
#include "ast.h"
#include "expression.h"
#include "interpreter.h"

using namespace std;

struct CodeBuffer {
	size_t size;
	char* code;

	CodeBuffer(size_t size) {
		this->size = size;
		this->code = new char[size+1];
		this->code[size] = '\0';
	}

	char* end() {
		return this->code + size;
	}
};

CodeBuffer load_buffer(char* filename) {
	FILE* f = fopen(filename, "rb");
	if (f==NULL) {
		cerr << "Unable to open file!\n";
		throw 0;
	}
	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);
	CodeBuffer buffer(len);
	fread(buffer.code, buffer.size, 1, f);
	fclose(f);
	return buffer;
}

struct MatcherEntry {
	string type;
	TokenMatch::Matcher::Ptr matcher;
};

const std::string ansi_red = "\e[0;31m";
const std::string ansi_yel = "\e[0;33m";
const std::string ansi_cya = "\e[0;36m";
const std::string ansi_rst = "\e[0m";

int main(int argc, char** argv) {

	// printf("File arg: %s\n", argv[1]);

	CodeBuffer buffer = load_buffer(argv[1]);

	auto tokens = Lexer::parse_buffer(buffer.code, buffer.end());

	// int no = 0;
	// for (auto it=tokens.begin(); it<tokens.end(); ++it) {
		// cout << ++no << ".\t" << it->format() << endl;
	// }

	TokenMatch::Token* ptr = tokens.data();

	ptr = tokens.data();
	auto ast = AST::create_ast(ptr);

	// cout << ast->format() << endl;

	Interpreter::run(ast);

	return 0;
}
