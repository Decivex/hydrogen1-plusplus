#include "expression.h"

#include <vector>
#include <iostream>
#include <string>

namespace Expression {
	IntValue::IntValue(int value) : value(value) {}

	std::string IntValue::format() {
		return std::to_string(value);
	}

	int IntValue::get_integer(Context* ctx) {
		return value;
	}

	Variable::Variable(std::string name) : name(name) {}

	std::string Variable::format() {
		return name;
	}

	int Variable::get_integer(Context* ctx) {
		return ctx->get_integer(name);
	}

	UnaryOp::UnaryOp(Node::Ptr child, Operator::Type type) : child(child), type(type) {}

	std::string UnaryOp::format() {
		return "(" + Operator::symbol(type) + child->format() + ")";
	}

	int UnaryOp::get_integer(Context* ctx) {
		return Operator::operation(type)(0, child->get_integer(ctx));
	}

	BinaryOp::BinaryOp(Node::Ptr left, Node::Ptr right, Operator::Type type)
		: left(left), right(right), type(type) {}
	
	std::string BinaryOp::format() {
		return "(" + left->format() + Operator::symbol(type) + right->format() + ")";
	}

	int BinaryOp::get_integer(Context* ctx) {
		return Operator::operation(type)(left->get_integer(ctx), right->get_integer(ctx));
	}

	FunctionCall::FunctionCall(std::string name, Node::Ptr value) : name(name), value(value) {}

	std::string FunctionCall::format() {
		return "call:" + name + value->format();
	}

	int FunctionCall::get_integer(Context* ctx) {
		return ctx->get_function(name)->int_call(value->get_integer(ctx));
	}

	Operator::Type get_operator(TokenMatch::Token* token) {
		if (token->type==&Tokens::OPERATOR) {
			return std::any_cast<Operator::Type>(token->data);
		}
		else {
			throw "not an operator token";
		}
	}

	Operator::Type get_operator(TokenMatch::Match match) {
		return get_operator(match.ptr);
	}

	Node::Ptr prepend_unary_operators(Node::Ptr node, TokenMatch::Token* ptr, TokenMatch::Token* first) {
		while (ptr >= first) {
			Operator::Type op = get_operator(ptr);
			node = Node::Ptr(new UnaryOp(node, op));
			ptr--;
		}
		return node;
	}

	Node::Ptr parse_expression_value(TokenMatch::Match match) {
		TokenMatch::Token* ptr = match.ptr;

		size_t unary_op_count = 0;
		while (ptr->type==&Tokens::OPERATOR) {
			ptr++;
			unary_op_count++;
		}

		Node::Ptr node;
		
		std::string tag = match.capture(0).tag;
		
		if (tag == "integer") {
			int val = std::stoi(std::any_cast<std::string>(ptr->data));
			node = Node::Ptr(new IntValue(val));
		}
		else if (tag == "variable") {
			std::string name = std::any_cast<std::string>(ptr->data);
			node = Node::Ptr(new Variable(name));
		}
		else if (tag == "sub_expr") {
			node = parse_expression(match.capture(0));
		}
		else if (tag == "function_call") {
			std::string name = std::any_cast<std::string>(ptr->data);
			auto val = parse_expression(match.capture(0).capture(0));
			node = Node::Ptr(new FunctionCall(name, val));
		}
		else {
			std::cout << "ep: unkown value type" << std::endl;
			return Node::Ptr();
		}
		
		return prepend_unary_operators(node, ptr-1, match.ptr);
	}

	void dbg(std::string s) {
		std::cout << s << std::endl;
	}

	Node::Ptr order_nodes(Operator::Type* operators, size_t operator_count, Node::Ptr* values, size_t value_count) {
		if (operator_count==0) {
			return values[0];
		}

		if (operator_count==1) {
			return Node::Ptr(new BinaryOp(values[0], values[1], operators[0]));
		}

		size_t highest_priority = Operator::order(operators[0]);
		size_t highest_priority_idx = 0;

		for (size_t idx=1; idx<operator_count; idx++) {
			int order = Operator::order(operators[idx]);
			if (order >= highest_priority) {
				highest_priority = order;
				highest_priority_idx = idx;
			}
		}

		// dbg("Highest priority: " + operators[highest_priority_idx]->symbol);

		size_t left_value_count = highest_priority_idx + 1;
		Node::Ptr* left_values = values;

		size_t right_value_count = value_count-highest_priority_idx;
		Node::Ptr* right_values = values + left_value_count;

		size_t left_operator_count = highest_priority_idx;
		Operator::Type* left_operators = operators;

		size_t right_operator_count = operator_count-left_operator_count-1;
		Operator::Type* right_operators = operators + highest_priority_idx + 1;

		Node::Ptr left = order_nodes(left_operators, left_operator_count, left_values, left_value_count);
		Node::Ptr right = order_nodes(right_operators, right_operator_count, right_values, right_value_count);

		return Node::Ptr(new BinaryOp(left, right, operators[highest_priority_idx]));
	}

	Node::Ptr parse_expression(TokenMatch::Match match) {
		size_t capture_count = match.capture_list.size();
		size_t operator_count = (capture_count - 1) / 2;
		size_t value_count = operator_count + 1;
		

		Operator::Type* operators = new Operator::Type[operator_count];
		for (size_t i=0; i<operator_count; i++) {
			auto capture = match.capture(i*2+1);
			operators[i] = get_operator(capture);
		}

		Node::Ptr* values = new Node::Ptr[value_count];
		for (size_t i=0; i<value_count; i++) {
			auto capture = match.capture(i*2);
			values[i] = parse_expression_value(capture);
		}

		auto node = order_nodes(operators, operator_count, values, value_count);


		delete operators;

		return node;
	}
}