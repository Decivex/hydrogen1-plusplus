#include "string_matching.h"

#include <iostream>
#include <cctype>

#include "operator.h"

namespace StringMatch {
	std::string Match::data() {
		return std::string(ptr, len);
	}

	std::string Match::format() {
		std::string str = "\"" + data() + "\"";

		if (!capture_list.empty()) {
			str += "[";
			str += std::to_string(capture_list.size());
			str += "](";

			for (size_t i=0; i<capture_list.size(); i++) {
				auto capture = (Match*)capture_list[i].get();
				str += capture->format();


				if (i<capture_list.size()-1) {
					str += ", ";
				}
			}

			str += ")";
		}

		return str;
	}

	std::string Match::capture_data(size_t idx) {
		return ((Match*)capture_list[idx].get())->data();
	}

	String::String(std::string str) : str(str) {}

	Match String::match(char* ptr) {
		for (size_t i = 0; i<str.size(); i++) {
			if (str[i] != ptr[i]) {
				return Match(ptr, 0);
			}
		}
		return Match(ptr, str.size());
	}

	CharList::CharList(std::string chars) {
		this->char_set = std::set<char>(chars.begin(), chars.end());
	}

	Match CharList::match(char* ptr) {
		if (char_set.find(*ptr) != char_set.end()) {
			return Match(ptr, 1);
		}
		else {
			return Match(ptr, 0);
		}
	}

	Matcher::Ptr character(char c) {
		return std::make_shared<Pattern::Equal<char,Match>>(c);
	}

	Matcher::Ptr string(std::string str) {
		return std::make_shared<String>(str);
	}

	Matcher::Ptr char_list(std::string characters) {
		return std::make_shared<CharList>(characters);
	}

	Matcher::Ptr word() {
		return std::make_shared<Pattern::Function<char,Match>>(
			[](char* ptr) {
				size_t len = 0;

				while (isalpha(ptr[len]) || ptr[len] == '_') len++;

				return len;
			}
		);
	}

	Matcher::Ptr no_newline() {
		return std::make_shared<Pattern::Function<char,Match>>(
			[](char* ptr) {
				if (*ptr == '\0') return 0;

				if (*ptr == '\n' || *ptr == '\r') {
					return 0;
				}
				else {
					return 1;
				}
			}
		);
	}

	Matcher::Ptr digit() {
		return std::make_shared<Pattern::Function<char,Match>>(
			[](char* ptr) {
				return (size_t)(isdigit(*ptr));
			}
		);
	}

	Matcher::Ptr operator_symbol() {
		std::vector<Matcher::Ptr> matchers;

		for (size_t i=0; i<Operator::OP_COUNT; i++) {
			matchers.push_back(string(Operator::symbol(i)));
		}

		return std::make_shared<Pattern::Longest<char,Match>>(matchers);
	}

	Matcher::Ptr repeat(int min, int max, Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Repeat<char,Match>>(min, max, matcher);
	}

	Matcher::Ptr repeat(int min, Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Repeat<char,Match>>(min, matcher);
	}

	Matcher::Ptr sequence(std::initializer_list<Matcher::Ptr> matchers) {
		return std::make_shared<Pattern::Sequence<char,Match>>(matchers);
	}

	Matcher::Ptr capture(Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Capture<char,Match>>(matcher);
	}

	Matcher::Ptr nop(Matcher::Ptr matcher) {
		return matcher;
	}

	Matcher::Ptr any_char() {
		return std::make_shared<Pattern::Function<char,Match>>(
			[](char* ptr) {
				return (size_t)(*ptr!='\0');
			}
		);
	}

	Matcher::Ptr none() {
		return std::make_shared<Pattern::Function<char,Match>>(
			[](char* ptr) {
				return 0;
			}
		);
	}
}