#include "token_matching.h"

namespace TokenMatch {
	std::string Match::format(size_t indent) {
		std::string str;

		str += "<<";

		for (size_t i=0; i<len; i++) {
			str += ptr[i].format();

			if (i<len-1) {
				str += ", ";
			}
		}

		str += ">>";
		
		if (!capture_list.empty()) {
			str += "\n";

			for (size_t i=0; i<capture_list.size(); i++) {
				std::string cap_str(indent, '\t');

				cap_str += "[" + std::to_string(i);

				if (!capture_list[i]->tag.empty()) {
					cap_str += ":" + capture_list[i]->tag;
				}

				cap_str += "] ";

				Match* capture = (Match*)capture_list[i].get();
				cap_str += capture->format(indent+1);

				cap_str += "\n";
				str += cap_str;
			}
		}


		return str;
	}

	Match Match::capture(size_t idx) {
		Match* match = (Match*)capture_list[idx].get();
		return *match;
	};

	std::any Match::data(size_t idx) {
		auto token = ptr + idx;
		return token->data;
	}

	Any::Any(){}

	Match Any::match(Token* ptr) {
		if (ptr->type == nullptr) {
			return Match(ptr, 0);
		}

		if (ptr->type == &Tokens::END) {
			return Match(ptr, 0);
		}

		return Match(ptr, 1);
	}

	Type::Type(TokenType type) : type(type) {}

	Match Type::match(Token* ptr) {
		Match match = Any::match(ptr);

		if (match.is_match() && ptr->type == this->type) {
			return match;
		}

		return Match(ptr, 0);
	}

	String::String(TokenType type, std::string str) : Type(type), str(str) {}

	Match String::match(Token* ptr) {
		Match match = Type::match(ptr);

		if (!match.is_match()) {
			return Match(ptr, 0);
		}

		try {
			std::string token_str = std::any_cast<std::string>(ptr->data);

			if (token_str == str) {
				return match;
			}
		}
		catch (const std::bad_any_cast& e) {
			// Ignore cast error and return empty match
		}

		return Match(ptr, 0);
	}

	Matcher::Ptr type(TokenType type) {
		return std::make_shared<Type>(type);
	}

	Matcher::Ptr string(TokenType type, std::string str) {
		return std::make_shared<String>(type, str);
	}

	Matcher::Ptr sequence(std::initializer_list<Matcher::Ptr> matchers) {
		return std::make_shared<Pattern::Sequence<Token,Match>>(matchers);
	}

	Matcher::Ptr capture(Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Capture<Token,Match>>(matcher);
	}

	Matcher::Ptr capture(std::string tag, Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Capture<Token,Match>>(tag, matcher);
	}

	Matcher::Ptr repeat(int min, int max, Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Repeat<Token,Match>>(min, max, matcher);
	}

	Matcher::Ptr repeat(int min, Matcher::Ptr matcher) {
		return std::make_shared<Pattern::Repeat<Token,Match>>(min, matcher);
	}

	Matcher::Ptr longest(std::initializer_list<Matcher::Ptr> matchers) {
		return std::make_shared<Pattern::Longest<Token,Match>>(matchers);
	}

	Matcher::Ptr lambda(std::function<size_t(Token*)> func) {
		return std::make_shared<Pattern::Function<Token,Match>>(func);
	}

	Matcher::Ptr unary_op() {
		return std::make_shared<Pattern::Function<Token,Match>>(
			[](Token* ptr){
				if (ptr->type!=&Tokens::OPERATOR) {
					return 0;
				}

				Operator::Type op = std::any_cast<Operator::Type>(ptr->data);

				if (Operator::TRAITS[(int)op] & Operator::UNARY) {
					return 1;
				}

				return 0;
			}
		);
	}

	Matcher::Ptr binary_op() {
		return std::make_shared<Pattern::Function<Token,Match>>(
			[](Token* ptr){
				if (ptr->type!=&Tokens::OPERATOR) {
					return 0;
				}

				Operator::Type op = std::any_cast<Operator::Type>(ptr->data);

				if (Operator::TRAITS[(int)op] & Operator::BINARY) {
					return 1;
				}

				return 0;
			}
		);
	}

	Matcher::Ptr in_parens(Matcher::Ptr matcher) {
		return sequence({
			type(&Tokens::PAREN_OPEN), matcher, type(&Tokens::PAREN_CLOSE)
		});
	}

	Matcher::Ptr expr_value() {
		return capture(sequence({
			repeat(0, unary_op()),
			longest({
				capture("variable", type(&Tokens::IDENTIFIER)),
				capture("integer", type(&Tokens::INTEGER)),
				capture("sub_expr", sequence({
					type(&Tokens::PAREN_OPEN),
					TokenMatch::Matcher::Ptr(new Pattern::Lambda<Token,Match>([](Token* ptr){
						return expression()->match(ptr);
					})),
					type(&Tokens::PAREN_CLOSE)
				})),
				capture("function_call", sequence({
					type(&Tokens::IDENTIFIER),
					type(&Tokens::PAREN_OPEN),
					capture(TokenMatch::Matcher::Ptr(new Pattern::Lambda<Token,Match>([](Token* ptr){
						return expression()->match(ptr);
					}))),
					type(&Tokens::PAREN_CLOSE)
				}))
			})
		}));
	}

	Matcher::Ptr expression() {
		auto xpval = expr_value();
		return sequence({
			xpval, repeat(0, sequence({
				capture(binary_op()), xpval
			}))
		});
	}
}