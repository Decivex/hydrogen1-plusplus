#include "tokens.h"

#include <cctype>

#include <iostream>


namespace Tokens {
	Token::Token() : type(nullptr), source(nullptr) {}

	Token::Token(const TypePtr type) : type(type), source(nullptr) {}

	Token::Token(const TypePtr type, std::any data, char* source)
		: type(type), data(data), source(source) {};

	const std::string ansi_red = "\e[0;31m";
	const std::string ansi_yel = "\e[0;33m";
	const std::string ansi_cya = "\e[0;36m";
	const std::string ansi_rst = "\e[0m";

	int isword(int chara) {
		return isalpha(chara) || chara == '_';
	}

	int no_newline(int chara) {
		return chara != '\n' && chara != '\r';
	}

	auto default_format = [] (const Token& token) {
		return token.type->name;
	};

	auto string_format = [] (const Token& token) {
		return token.type->name + ansi_yel + " \"" + std::any_cast<std::string>(token.data) + "\"" + ansi_rst;
	};

	auto operator_format = [] (const Token& token) {
		Operator::Type type = std::any_cast<Operator::Type>(token.data);
		return token.type->name + " " + ansi_yel + Operator::symbol(type) + ansi_rst;
	};

	auto no_data = [] (StringMatch::Match& match) { return std::any(); };
	
	auto string_data = [] (StringMatch::Match& match) {
		return match.capture_data(0);
	};

	auto operator_data = [] (StringMatch::Match& match) {
		std::string str = match.capture_data(0);

		for (size_t i=0; i<Operator::OP_COUNT; i++) {
			if (Operator::symbol(i) == str) {
				return (Operator::Type)i;
			}
		}

		// TBD: Proper exception
		throw "unknown operator";
	};


	const Type IDENTIFIER = {
		.name = "identifier",
		.format = string_format,
		.matcher = StringMatch::capture(StringMatch::word()),
		.data = string_data,
		.repr = [] (Token& token) { return std::any_cast<std::string>(token.data); }
	};

	const Type WHITESPACE = {
		.name = "whitespace",
		.ignore = true,
		.format = default_format,
		.matcher = StringMatch::repeat(1, StringMatch::char_list(" \t")),
		.data = no_data,
		.repr = [] (Token& token) { return ""; }
	};

	const Type ASSIGNMENT = {
		.name = "assignment",
		.format = default_format,
		.matcher = StringMatch::character('='),
		.data = no_data,
		.repr = [] (Token& token) { return " = "; }
	};

	const Type KEYWORD = {
		.name = "keyword",
		.format = string_format,
		.matcher = StringMatch::sequence({StringMatch::character('@'), IDENTIFIER.matcher}),
		.data = string_data,
		.repr = [] (Token& token) { return ansi_cya+"@"+std::any_cast<std::string>(token.data)+ansi_rst+" "; }
	};

	const Type COMMENT = {
		.name = "comment",
		.ignore = true,
		.format = string_format,
		.matcher = StringMatch::sequence({
			StringMatch::character('#'),
			StringMatch::repeat(0, StringMatch::char_list(" \t")),
			StringMatch::capture(StringMatch::repeat(0, StringMatch::no_newline()))
		}),
		.data = string_data,
		.repr = [] (Token& token) { return ""; }
	};

	const Type NEWLINE = {
		.name = "newline",
		.ignore = true,
		.format = default_format,
		.matcher = StringMatch::repeat(1, StringMatch::char_list("\n\r")),
		.data = no_data,
		.repr = [] (Token& token) { return "↵\n"; }
	};

	const Type INTEGER = {
		.name = "integer",
		.format = string_format,
		.matcher = StringMatch::capture(StringMatch::repeat(1, StringMatch::digit())),
		.data = string_data,
		.repr = [] (Token& token) { return ansi_yel+"i"+std::any_cast<std::string>(token.data)+ansi_rst; }
	};

	const Type OPERATOR = {
		.name = "operator",
		.format = operator_format,
		.matcher = StringMatch::capture(StringMatch::operator_symbol()),
		.data = operator_data,
		.repr = [] (Token& token) {
			Operator::Type type = std::any_cast<Operator::Type>(token.data);
			return ansi_yel+" "+Operator::symbol(type)+" "+ansi_rst;
		}
	};

	const Type PAREN_OPEN = {
		.name = "paren_open",
		.format = default_format,
		.matcher = StringMatch::character('('),
		.data = no_data,
		.repr = [] (Token& token) { return "("; }
	};

	const Type PAREN_CLOSE = {
		.name = "paren_close",
		.format = default_format,
		.matcher = StringMatch::character(')'),
		.data = no_data,
		.repr = [] (Token& token) { return ")"; }
	};

	const Type NONE = {
		.name = "none",
		.format = default_format,
		.matcher = StringMatch::capture(StringMatch::any_char()),
		.data = string_data,
		.repr = [] (Token& token) { return ""; }
	};

	const Type END = {
		.name = "eof",
		.format = default_format,
		.matcher = StringMatch::none(),
		.data = no_data,
		.repr = [] (Token& token) { return "EOF"; }
	};

	std::string Token::format() const {
		if (type == nullptr) return "(null)";
		return type->format(*this);
	}

	Token Token::operator=(const Token& token) {
		return Token{
			.type = token.type,
			.data = token.data,
			.source = token.source
		};
	}

	Token create_token(char* source, TypePtr type, StringMatch::Match& match) {
		return Token{
			.type = type,
			.data = type->data(match),
			.source = source
		};
	}
}