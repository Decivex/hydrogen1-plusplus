#pragma once

#include <memory>
#include <string>

#include "token_matching.h"
#include "operator.h"

namespace Expression {
	class Function {
	public:
		virtual int int_call(int argument) = 0;
	};

	class Context {
	public:
		virtual int get_integer(std::string varname) = 0;
		virtual Function* get_function(std::string funcname) = 0;
		virtual void set_integer(std::string varname, int value) = 0;
	};

	class Node {
	public:
		typedef std::shared_ptr<Node> Ptr;
		virtual std::string format() = 0;
		virtual int get_integer(Context* ctx) = 0;
	};

	class IntValue : public Node {
	public:
		IntValue(int value);
		int value;
		std::string format();
		int get_integer(Context* ctx);
	};

	class Variable : public Node {
	public:
		Variable(std::string name);
		std::string name;
		std::string format();
		int get_integer(Context* ctx);
	};

	class UnaryOp : public Node {
	public:
		UnaryOp(Node::Ptr child, Operator::Type type);
		std::string format();
		int get_integer(Context* ctx);
	private:
		Node::Ptr child;
		Operator::Type type;
	};

	class BinaryOp : public Node {
	public:
		BinaryOp(Node::Ptr left, Node::Ptr right, Operator::Type type);
		std::string format();
		int get_integer(Context* ctx);
	private:
		Node::Ptr left;
		Node::Ptr right;
		Operator::Type type;
	};

	class FunctionCall : public Node {
	public:
		FunctionCall(std::string name, Node::Ptr value);
		std::string format();
		int get_integer(Context* ctx);
	private:
		std::string name;
		Node::Ptr value;
	};

	Node::Ptr parse_expression(TokenMatch::Match match);
}

