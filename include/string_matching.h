#pragma once

#include "pattern_matching.h"

#include <string>
#include <set>

namespace StringMatch {
	struct Match : public Pattern::Match<char> {
		using Pattern::Match<char>::Match;

		std::string data();

		std::string format();

		std::string capture_data(size_t idx);
	};

	typedef Pattern::Matcher<char,Match> Matcher;

	class String : public Pattern::Matcher<char,Match> {
	public:
		String(std::string);
		Match match(char* ptr);
	private:
		std::string str;
	};

	class CharList : public Pattern::Matcher<char,Match> {
	public:
		CharList(std::string characters);
		Match match(char* ptr);
	private:
		std::set<char> char_set;
	};

	Matcher::Ptr character(char c);
	Matcher::Ptr string(std::string);
	Matcher::Ptr char_list(std::string);
	Matcher::Ptr word();
	Matcher::Ptr no_newline();
	Matcher::Ptr digit();
	Matcher::Ptr operator_symbol();
	Matcher::Ptr repeat(int,int,Matcher::Ptr);
	Matcher::Ptr repeat(int,Matcher::Ptr);
	Matcher::Ptr sequence(std::initializer_list<Matcher::Ptr>);
	Matcher::Ptr capture(Matcher::Ptr);
	Matcher::Ptr nop(Matcher::Ptr);
	Matcher::Ptr any_char();
	Matcher::Ptr none();
}