#pragma once

#include <array>
#include <cstddef>
#include <string>
#include <map>
#include <functional>

namespace Operator {
	enum class Type {
		ADD, SUB, MUL, DIV, LT, GT, LTE, GTE, EQ, NE
	};

	enum Traits {
		UNARY = 1<<0,
		BINARY = 1<<1
	};

	const int OP_COUNT = 10;
	const std::string SYMBOL[] = {
		"+", "-", "*", "/", "<", ">", "<=", ">=", "==", "!="
	};

	const int ORDER[OP_COUNT] = {
		2, 2, 1, 1, 0, 0, 0, 0, 0, 0
	};

	std::string symbol(size_t idx);
	std::string symbol(Type type);

	int order(Type type);

	const int TRAITS[] = {
		UNARY|BINARY,
		UNARY|BINARY,
		BINARY,
		BINARY,
		BINARY,
		BINARY,
		BINARY,
		BINARY,
		BINARY,
		BINARY
	};

	typedef std::function<int(int,int)> OperatorFunc;

	OperatorFunc operation(Type type);
}
