#pragma once

#include <exception>
#include <map>
#include <string>
#include <functional>

#include "ast.h"
#include "expression.h"

namespace Interpreter {
	class LambdaFunction : public Expression::Function {
	public:
		LambdaFunction(std::function<int(int)> func);
		int int_call(int argument);
	private:
		std::function<int(int)> func;
	};

	class ScopeInstance : public Expression::Context {
	public:
		ScopeInstance(AST::Scope::Ptr scope, ScopeInstance* parent);

		void run();

		int get_integer(std::string varname);
		Expression::Function* get_function(std::string funcname);
		void set_integer(std::string varname, int value);

		std::string format();

	private:
		AST::Scope::Ptr scope;
		ScopeInstance* parent;
		std::map<std::string,int> integers;
	};
	
	void run(AST::Scope::Ptr scope);
}