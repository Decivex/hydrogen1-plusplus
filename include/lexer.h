#pragma once

#include <vector>

#include "tokens.h"

namespace Lexer {
	struct TokenList {
		Tokens::Token* list;
		size_t capacity;

		TokenList();
		~TokenList();
	};

	std::vector<Tokens::Token> parse_buffer(char* cursor, char* end);
}