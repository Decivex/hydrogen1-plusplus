#pragma once

#include <cstddef>
#include <vector>
#include <memory>
#include <functional>
#include <string>

namespace Pattern {
	template <class C>
	struct Match;

	template <class C>
	using MatchPtr = std::shared_ptr<Match<C>>;

	template <class C>
	struct Match {
		Match(C* data, size_t len) : ptr(data), len(len), empty_match(false) {};
		Match(size_t len) : Match(false, len) {};
		Match(bool empty_match, size_t len) : empty_match(empty_match), len(len) {}

		void merge(Match<C>* match) {
			this->len += match->len;
			capture_list.insert(capture_list.end(), match->capture_list.begin(), match->capture_list.end());
		};
		
		bool is_match() {
			return empty_match || (len > 0);
		};

		Match capture(size_t idx) {
			return capture_list[idx];
		};

		bool empty_match;
		size_t len;
		C* ptr;
		std::vector<MatchPtr<C>> capture_list;
		std::string tag;
	};

	template <class T, class M>
	class Matcher {
	public:
		virtual M match(T*) = 0;

		typedef std::shared_ptr<Matcher> Ptr;
	};

	template <class T, class M>
	using MatcherPtr = std::shared_ptr<Matcher<T,M>>;


	template <class T, class M>
	class Equal : public Matcher<T,M> {
	public:
		Equal(T value) : value(value) {}
		M match(T* ptr) {
			return M(ptr, (size_t)(*ptr == value));
		}
	private:
		T value;
	};

	template <class T, class M>
	class None : public Matcher<T,M> {
	public:
		None() {};
		M match(T* ptr) {
			return M(0);
		}
	};

	template <class T, class M>
	class Repeat : public Matcher<T,M> {
	public:
		Repeat(int min, int max, MatcherPtr<T,M> matcher)
			: min(min), max(max), matcher(matcher) {};
		Repeat(int min, MatcherPtr<T,M> matcher)
			: min(min), max(-1), matcher(matcher) {};
		M match(T* ptr) {
			M total(ptr, 0);
			total.empty_match = true;
			for (int count=0; max>-1?count<max:true; count++) {
				M match = matcher->match(ptr);
				if (match.is_match()) {
					total.merge(&match);
					ptr += match.len;
				}
				else {
					return count >= min?total:M(0);
				}
			}
			return total;
		}
	private:
		int min;
		int max;
		MatcherPtr<T,M> matcher;
	};

	template <class T, class M>
	class Sequence : public Matcher<T,M> {
	public:
		Sequence(std::initializer_list<typename Matcher<T,M>::Ptr> matchers) {
			matcher_list = std::vector<typename Matcher<T,M>::Ptr>(matchers);
		}

		M match(T* ptr) {
			M total(ptr, 0);
			for (auto it=matcher_list.begin(); it<matcher_list.end(); it++) {
				auto match = (*it)->match(ptr);
				if (match.is_match()) {
					ptr += match.len;
					total.merge(&match);
				}
				else {
					return M(ptr, 0);
				}
			}
			return total;
		}
	private:
		std::vector<typename Matcher<T,M>::Ptr> matcher_list;
	};

	template <class T, class M>
	class Longest : public Matcher<T,M> {
	public:
		Longest(std::initializer_list<typename Matcher<T,M>::Ptr> matchers) {
			matcher_list = std::vector<typename Matcher<T,M>::Ptr>(matchers);
		}

		Longest(std::vector<typename Matcher<T,M>::Ptr> matchers) : matcher_list(matchers) {}

		M match(T* ptr) {
			M total(ptr, 0);
			for (auto it=matcher_list.begin(); it<matcher_list.end(); it++) {
				auto match = (*it)->match(ptr);
				if (match.is_match() && match.len > total.len) {
					total = match;
				}
			}
			return total;
		}
	private:
		std::vector<typename Matcher<T,M>::Ptr> matcher_list;
	};

	template <class T, class M>
	class Capture : public Matcher<T,M> {
	public:
		Capture(std::string tag, MatcherPtr<T,M> matcher) : tag(tag), matcher(matcher) {};
		Capture(MatcherPtr<T,M> matcher) : tag(), matcher(matcher) {};

		M match(T* ptr) {
			M match = matcher->match(ptr);
			match.tag = tag;
			M new_match(ptr, match.len);
			new_match.capture_list.push_back(std::shared_ptr<M>(new M(match)));
			return new_match;
		}
	private:
		typename Matcher<T,M>::Ptr matcher;
		std::string tag;
	};

	template <class T, class M>
	class Function : public Matcher<T,M> {
	public:
		Function(std::function<size_t(T*)> func) : func(func) {};
		M match(T* ptr) {
			size_t result = func(ptr);
			return M(ptr, result);

		}
	private:
		std::function<size_t(T*)> func;
	};

	template <class T, class M>
	class Lambda : public Matcher<T,M> {
	public:
		Lambda(std::function<M(T*)> func) : func(func) {};
		M match(T* ptr) {
			return func(ptr);
			
		}
	private:
		std::function<M(T*)> func;
	};
}