#pragma once
#include <string>
#include <functional>
#include <memory>
#include <any>
#include <array>

#include "operator.h"
#include "string_matching.h"

namespace Tokens {
	class Type;
	typedef const Type* TypePtr;

	struct Token {
		Token();
		Token(const TypePtr type);
		Token(const TypePtr type, std::any data, char* source);

		const TypePtr type;
		std::any data;
		char* source;

		std::string format() const;
		Token operator=(const Token&);
	};
	
	struct Type {
		std::string name;
		bool ignore;
		std::function<std::string (const Token&)> format;
		StringMatch::Matcher::Ptr matcher;
		std::function<std::any(StringMatch::Match&)> data;
		std::function<std::string (Token&)> repr;

		Type(const TypePtr) = delete;
	};

	extern const Type IDENTIFIER;
	extern const Type WHITESPACE;
	extern const Type ASSIGNMENT;
	extern const Type KEYWORD;
	extern const Type COMMENT;
	extern const Type NEWLINE;
	extern const Type INTEGER;
	extern const Type OPERATOR;
	extern const Type PAREN_OPEN;
	extern const Type PAREN_CLOSE;
	
	extern const Type NONE;
	extern const Type END;

	Token create_token(char* source, const TypePtr type, StringMatch::Match& match);
}