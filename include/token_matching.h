#pragma once

#include <string>
#include <any>

#include "tokens.h"
#include "pattern_matching.h"

namespace TokenMatch {
	typedef const Tokens::Token Token;
	typedef const Tokens::TypePtr TokenType;

	struct Match : public Pattern::Match<Token> {
		using Pattern::Match<Token>::Match;

		Match capture(size_t idx);

		std::string format(size_t indent=0);
		std::any data(size_t idx);
	};
	
	typedef Pattern::Matcher<Token,Match> Matcher;

	class Any : public Matcher {
	public:
		Any();
		Match match(Token* ptr);
	};

	class Type : public Any {
	public:
		Type(TokenType type);
		Match match(Token* ptr);
	private:
		TokenType type;
	};

	class String : public Type {
	public:
		String(TokenType type, std::string str);
		Match match(Token* ptr);
	private:
		std::string str;
	};

	Matcher::Ptr type(TokenType type);
	Matcher::Ptr string(TokenType type, std::string str);
	Matcher::Ptr sequence(std::initializer_list<Matcher::Ptr> matchers);
	Matcher::Ptr repeat(int min, Matcher::Ptr matchers);
	Matcher::Ptr repeat(int min, int max, Matcher::Ptr matchers);
	Matcher::Ptr capture(Matcher::Ptr matcher);
	Matcher::Ptr capture(std::string tag, Matcher::Ptr matcher);
	Matcher::Ptr expr_value();
	Matcher::Ptr expression();
}