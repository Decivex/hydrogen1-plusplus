#pragma once

#include <string>
#include <memory>
#include <vector>
#include <any>

#include "token_matching.h"
#include "expression.h"

namespace AST {
	class Statement {
	public:
		typedef std::shared_ptr<Statement> Ptr;
		virtual std::string format(size_t indent=0) = 0;
	};

	class VarDeclaration : public Statement {
	public:
		VarDeclaration(std::string name, Expression::Node::Ptr value);
		std::string format(size_t indent=0);
		std::string name;
		Expression::Node::Ptr value;
	};

	enum class ScopeType {
		BLOCK, FILE, FUNCTION, LOOP, CONDITIONAL
	};
	extern std::string SCOPE_TYPE_NAMES[];

	struct Scope {
		Scope(ScopeType type);
		ScopeType type;
		std::vector<Statement::Ptr> statements;

		void add_statement(Statement* statement);

		std::string format(size_t indent=0);

		typedef std::shared_ptr<Scope> Ptr;

		Scope::Ptr parent;
	};

	class Conditional : public Statement {
	public:
		Conditional(Expression::Node::Ptr condition);
		Scope::Ptr scope;
		std::string format(size_t indent=0);
		Expression::Node::Ptr condition;
	};

	class FunctionCall : public Statement {
	public:
		FunctionCall(std::string name, Expression::Node::Ptr value);
		std::string format(size_t indent=0);
		std::string name;
		Expression::Node::Ptr value;
	};

	Scope::Ptr create_ast(TokenMatch::Token* token_buffer);
}
